FROM alpine:3.8

RUN apk add --update build-base jsoncpp-dev linux-pam-dev curl-dev

ADD src /src
WORKDIR /src
VOLUME /export


CMD make && cp pam_jsonapi.so /export/pam_jsonapi.so
