# PAM_JSONAPI

This PAM module enables linux authentication via JSON-API. The API has to meet the following conditions:

 * Authentications paths must be in the shape of ```http(s)://<base_uri>/<username>/<sha256-password-hash>```
 * Each of that paths must return a JSON object like:
   ```json
   {
	   "uid": 1000,
	   "gid": 1000,
	   "fullname": "Sascha Winkelhofer",
	   "home": "/home/swinkelhofer",
	   "loginShell": "/bin/bash"
   }
   ```

## Build PAM-module

 * At first clone this git repo
 * Be aware to install all build dependencies before building the module.
 * Build:
   ```bash
   cd <Repo_Dir>
   export BASE_URI=<base_uri>
   make
   sudo make install
   ```

## Build dependencies

 * libcurl-openssl-dev
 * libjsoncpp-dev
 * make
 * g++
 * libpam0g-dev

## Runtime dependencies

 * libcurl3
 * libjsoncpp1-dev
 * make (for ```make install```)
 * gettext (for ```make install```)