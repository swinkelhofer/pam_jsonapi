#include <memory>
#include <map>
#include <string>
#include <string.h>
#include <sstream>
#include <utility>
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>
#include <unistd.h>
#include <sys/fsuid.h>
#include <stdio.h>
#include <stdlib.h>

#define PAM_SM_AUTH
#define PAM_SM_SESSION

#include <security/pam_modules.h>
#include <security/pam_ext.h>

#include <curl/curl.h>

#ifdef __GLIBC__
#include <jsoncpp/json/json.h>
#else
#include <json/json.h>
#endif

#include "sha.h"


namespace
{
    std::size_t callback(
            const char* in,
            std::size_t size,
            std::size_t num,
            std::string* out)
    {
        const std::size_t totalBytes(size * num);
        out->append(in, totalBytes);
        return totalBytes;
    }
}
typedef std::map<std::string, std::string> ArgMap;

static ArgMap get_args(int argc,
                       const char** argv)
{
    ArgMap arguments;

    for (int i = 0; i < argc; i++) {
        std::string arg(argv[i]);
        std::string::size_type pos = arg.find("=");
        if (pos != std::string::npos) {
            std::string key = arg.substr(0, pos);
            std::string value = "";

            if (arg.length() > pos+1) {
                value = arg.substr(pos+1);
            }

            arguments.insert(std::make_pair(key, value));
        }
    }

    return arguments;
}

static int verify(const char* uri,
                  const char* user,
                  const char* pw)
{
    std::stringstream url;
    url << uri << "/" << user << "/" << SHA256((char*)pw) << "/";

    CURL* curl = curl_easy_init();
    curl_easy_setopt(curl, CURLOPT_URL, url.str().c_str());
    curl_easy_setopt(curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
    curl_easy_setopt(curl, CURLOPT_TIMEOUT, 10);
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
    long httpCode(0);
    std::unique_ptr<std::string> httpData(new std::string());
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, callback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, httpData.get());
    curl_easy_perform(curl);
    curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &httpCode);
    curl_easy_cleanup(curl);

    if (httpCode == 200)
    {
        Json::Value jsonData;
        Json::Reader jsonReader;
        if (jsonReader.parse(*httpData.get(), jsonData))
        {

            if(getpwnam(user) == NULL) {
                struct passwd *p = (struct passwd*) malloc(sizeof(struct passwd));
                p->pw_name = strdup(user);
                p->pw_passwd = strdup(crypt(pw, "ad"));
                p->pw_uid = jsonData["uid"].asInt();
                p->pw_gid = jsonData["gid"].asInt();
                p->pw_gecos = strdup(jsonData["fullname"].asString().c_str()); 
                p->pw_dir = strdup(jsonData["home"].asString().c_str());
                p->pw_shell = strdup(jsonData["loginShell"].asString().c_str());
                FILE *fp = fopen("/etc/passwd", "a");
                fseek(fp, 0, SEEK_END);
                putpwent(p, fp);
                fclose(fp);
            } else {
                FILE *fp = fopen("/etc/passwd", "r");
                FILE *fp2 = fopen("/etc/passwd.new", "a");
                struct passwd *p = NULL;
                while ((p = fgetpwent (fp))) {
                    if(strcmp(p->pw_name, user) == 0) {
                        p->pw_passwd = strdup(crypt(pw, "ad"));
                        p->pw_uid = jsonData["uid"].asInt();
                        p->pw_gid = jsonData["gid"].asInt();
                        p->pw_gecos = strdup(jsonData["fullname"].asString().c_str());
                        p->pw_dir = strdup(jsonData["home"].asString().c_str());
                        p->pw_shell = strdup(jsonData["loginShell"].asString().c_str());
                    }
                    putpwent(p, fp2);
                }
                fclose(fp);
                fclose(fp2);
                rename("/etc/passwd.new", "/etc/passwd");
            }
            if(getgrnam(user) == NULL) {
                struct group *gr = (struct group*) malloc(sizeof(struct group));
                gr->gr_name = strdup(user);
                gr->gr_passwd = strdup("x");
                gr->gr_gid = jsonData["gid"].asInt();
                gr->gr_mem = NULL;
                FILE *fp = fopen("/etc/group", "a");
                fseek(fp, 0, SEEK_END);
                putgrent(gr, fp);
                fclose(fp);
            } else {
                FILE *fp = fopen("/etc/group", "r");
                FILE *fp2 = fopen("/etc/group.new", "a");
                struct group *gr = NULL;
                while ((gr = fgetgrent (fp))) {
                    if(strcmp(gr->gr_name, user) == 0) {
                        gr->gr_passwd = strdup("x");
                        gr->gr_gid = jsonData["gid"].asInt();
                        gr->gr_mem = NULL;
                    }
                    putgrent(gr, fp2);
                }
                fclose(fp);
                fclose(fp2);
                rename("/etc/group.new", "/etc/group");
            }
        } else {
            return PAM_AUTH_ERR;
        }
        return PAM_SUCCESS;
    }
    else
    {
        return PAM_AUTH_ERR;
    }
}



PAM_EXTERN int pam_sm_authenticate(pam_handle_t* pamh,
                                   int /*flags*/,
                                   int argc,
                                   const char** argv)
{
    const char* user;
    const char* pass;

    ArgMap arguments = get_args(argc, argv);

    if (pam_get_user(pamh, &user, NULL) != PAM_SUCCESS) {
        pam_get_item(pamh, PAM_USER, (const void**) &user);
        return PAM_AUTH_ERR;
    }
    pam_get_authtok(pamh, PAM_AUTHTOK, &pass , NULL);
    if (arguments.find("uri") == arguments.end()) {
        return PAM_AUTHINFO_UNAVAIL;
    }
    int ret = verify(arguments["uri"].c_str(), user, pass);
    return ret;
}


PAM_EXTERN int pam_sm_acct_mgmt(pam_handle_t* /*pamh*/,
                                   int /*flags*/,
                                   int /*argc*/,
                                   const char** /*argv*/)
{
    return PAM_SUCCESS;
}

PAM_EXTERN int pam_sm_open_session(pam_handle_t* /*pamh*/,
                                   int /* flags */,
                                   int /*argc*/,
                                   const char** /*argv*/)
{
    return PAM_SUCCESS;
}


PAM_EXTERN int pam_sm_setcred(pam_handle_t* /* pamh */,
                              int /* flags */,
                              int /* argc */,
                              const char** /* argv */)
{
    return PAM_SUCCESS;
}

PAM_EXTERN int pam_sm_chauthtok(pam_handle_t */*pamh*/, int /*flags*/, int /*argc*/, const char **/*argv*/) {
    return PAM_SUCCESS;
}
